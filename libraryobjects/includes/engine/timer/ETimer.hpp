/*
** EPITECH PROJECT, 2020
** qt-engine
** File description:
** ETimer
*/

#pragma once

#include "Object.hpp"
#include <QtCore/QTimer>

namespace libraryObjects {
	typedef Object<QTimer> ETimer;
}
