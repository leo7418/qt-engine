/*
** EPITECH PROJECT, 2020
** qt-engine
** File description:
** ELineEdit
*/

#pragma once

#include "Object.hpp"
#include <QtWidgets/QLineEdit>

namespace libraryObjects {
	typedef Object<QLineEdit> ELineEdit;
}
