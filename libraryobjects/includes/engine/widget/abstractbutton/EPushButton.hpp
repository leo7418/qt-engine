/*
** EPITECH PROJECT, 2020
** qt-engine
** File description:
** EPushButton
*/

#pragma once

#include "Object.hpp"
#include <QtWidgets/QPushButton>

namespace libraryObjects {
	typedef Object<QPushButton> EPushButton;
}
