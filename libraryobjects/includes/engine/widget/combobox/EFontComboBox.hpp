/*
** EPITECH PROJECT, 2020
** qt-engine
** File description:
** EFontComboBox
*/

#pragma once

#include "Object.hpp"
#include <QtWidgets/QFontComboBox>

namespace libraryObjects {
	typedef Object<QFontComboBox> EFontComboBox;
}
